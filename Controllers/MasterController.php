<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 16.07.16
 * Time: 21:13
 */

namespace Controller;


use Master\ControllerBase;
use Master\Generator;
use Master\ModelBase;
use Master\ViewBase;
use Master\MC;
use Models\ProductsModel;

class masterController extends ControllerBase
{
    public function indexAction()
    {
        MC::app()->set('layout', 'developer');
        return new ViewBase();
    }

    public function generatorAction()
    {
        $generator = new Generator();
        if($generator->modelGenerator('Products'))
            echo 'OK';
        else
            echo 'Error';
    }

    public function newAction()
    {
        $product = new ProductsModel();


        $product->CMSFetchMode($product::FETCH_OBJ);
        $p=$product->findAll();
        foreach ($p as $pp){
        echo $pp->getName();
        }
    }
}