<?php
/**
 * Doctrine console runner configuration
 * @since 2.0
 */
include('../vendor/autoload.php'); // Autoload  PSR-4 only
use Doctrine\ORM\Tools\Console\ConsoleRunner;
use McComponent\DoctrineComponent;
use Master\MC;

MC::app()->setConfig('config/db.php');
// replace with mechanism to retrieve EntityManager in your app
$doctrine = new DoctrineComponent();
$entityManager = $doctrine->getEntityManager();
return ConsoleRunner::createHelperSet($entityManager);
/**
 * AAA CNAME
 */