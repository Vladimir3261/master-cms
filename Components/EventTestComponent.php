<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 01.09.16
 * Time: 3:08
 */

namespace McComponent;


use Master\McComponent;
use Master\McComponentInterface;

class EventTestComponent extends McComponent implements McComponentInterface
{
    public function init()
    {
        // TODO: Implement init() method.
    }

    protected function sayHello($name)
    {
        return 'Hello '.$name;
    }
}