<?php

namespace McComponent;
use Master\McComponent;
use Master\Exception\CoreException;
use Master\MC;
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Annotations\AnnotationRegistry;

/**
 * Class DoctrineComponent settings for Doctrine ORM
 * and get doctrine entity manager
 * @package McComponent
 * @version 1.1
 * @since 2.0
 */
class DoctrineComponent extends McComponent implements \Master\McComponentInterface
{
    /**
     *
     * @var object ENTITY MANAGER 
     */
    private $em;
    /**
     * function init required to implement 
     * set config to doctrine ORM and set doctrine entity manager in $em
     * @return void
     * @throws CoreException
     */
    public function init()
    {
        $paths            = array('Models');
        $isDevMode        = true;
        $connectionParams = MC::app()->getConfig('db');
        //$cacheImpl = new \Doctrine\Common\Cache\FilesystemCache(ROOTDIR.'/cache');
        $config = Setup::createConfiguration($isDevMode);
       // $config->setMetadataCacheImpl($cacheImpl);
        $driver = new AnnotationDriver(new AnnotationReader(), $paths);
        // registering noop annotation autoloader - allow all annotations by default
        AnnotationRegistry::registerLoader('class_exists');
        $config->setMetadataDriverImpl($driver);
        //$cacheDriver = new \Doctrine\Common\Cache\ApcCache();
        //$cacheDriver->save('cache_id', 'my_data');
        $this->em = EntityManager::create($connectionParams, $config);
    }
    
    /**
     * @return \Doctrine\ORM\EntityManager
     */
    public function getEntityManager()
    {
        return $this->em;
    }
}

