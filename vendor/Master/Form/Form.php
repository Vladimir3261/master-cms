<?php
namespace Master\Form;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Form
 *
 * @author vladimir
 */
class Form
{
    protected $formParams = ['method' => 'post', 'action' => '/'];
    protected $formFields;
    protected $exists=NULL;

    /**
     * Form constructor.
     * @param $modelArray
     * @param $params
     * IN constructor write
     */
    public function __construct($modelArray, $params)
    {
        if($modelArray[$modelArray['primaryName']]) {
            $this->exists = [
                'name' => $modelArray['primaryName'],
                'value'=> $modelArray[$modelArray['primaryName']]
            ];
        }
       foreach($modelArray as $k=>$v)
        {
            if( isset($params[$k]) )
            {
                $this->formFields[$k] = $params[$k];
                $this->formFields[$k]['value'] = $v;
            }
        }
    }

    /**
     * show form field generated using this class build method
     * @param $name
     * @param null|array $params
     * @param null|array $selectors
     * @return string
     */
    public function show($name, $params=NULL, $selectors=NULL)
    {
        if($params)
        {
            foreach ($params as $param=>$value) {
                $this->formFields[$name][$param] = $value;
            }
        }
        return $this->build($name, $this->formFields[$name], $selectors);
    }

    /**
     * check to type
     * @param $name
     * @param $params
     * @param $selectors
     * @return bool|string
     */
    public function build($name, $params, $selectors=NULL)
    {
        if($params['type'] === 'textarea')
            return $this->getTextArea($name, $params);
        else if($params['type'] === 'checkbox')
            return $this->getCheckbox($name, $params);
        else if($params['type'] === 'radio')
            return $this->getRadio($name, $params);
        else if($params['type'] === 'hidden')
            return $this->getHidden($name, $params);
        else if($params['type'] === 'select')
            return $this->getSelect($name, $params, $selectors);
        else
            return $this->getInput($name, $params);
    }

    /**
     * creating checkboxes as array k=>$v
     * @param $name
     * @param $params
     * @return string
     */
    public function getCheckbox($name, $params)
    {
        $checked = ($params['value']) ? 'checked="checked"' : '';
        $str = '<input type="checkbox" '.$checked.'  name="'.$name.'" ';
        foreach ($params as $k=>$v){
            $str.=$k.'="'.$v.'" ';
        }
        $str .=' />';
        return $str;
    }

    public function getSelect($name, $params, $selectors)
    {
        unset($params['type']);
        $str = '<select name="'.$name.'" ';
        foreach($params as $k=>$v){
            $str.=$k.'="'.$v.'" ';
        }
        $str.='/>'."\r\n";
        foreach($selectors as $value=>$option)
        {
            $str.='<option value="'.$value.'">'.$option.'</option>'."\r\n";
        }
        $str.='</select>';
        return $str;
    }

    /**
     * creating radio as array k=>$v
     * @param $name
     * @param $params
     * @return string
     */
    public function getRadio($name, $params)
    {
        $str = '';
        foreach ($params as $k=>$v){
            $str = '<input type="radio" name="'.$name.'" value="'.$v.'" />';
        }
        return $str;
    }

    /**
     * text area input field
     * @param $name
     * @param $params
     * @return string
     */
    public function getTextArea($name, $params)
    {
        $val = isset($params['value']) ? $params['value'] : NULL;
        $str = '<textarea name="'.$name.'" ';
        foreach($params as $k=>$v){
            if($k!='value')
                $str .=$k.'="'.$v.'" ';
        }
        $str.='>'.$val.'</textarea>';
        return $str;
    }

    /**
     * simple text input
     * @param $name
     * @param $params
     * @return string
     */
    public function getInput($name, $params)
    {
        $str = '<input type="text" name="'.$name.'" ';
        foreach($params as $k=>$v){
            $str.=$k.'="'.$v.'" ';
        }
        $str.='/>';
        return $str;
    }

    /**
     * hidden field required to transfer this method 
     * params array key pairs name and value
     * @param $params
     * @return string
     */
    public function getHidden($params)
    {
        return '<input type="hidden" name="'.$params['name'].'" value="'.$params['value'].'" />';
    }

    /**
     * Submit button 
     * @param null $params
     * @return string
     */
    public function submit($params=NULL)
    {
        $str = '<input type="submit" ';
        if($params){
            foreach($params as $k=>$v)
            {
                $str.=$k.'="'.$v.'" ';
            }
        }
        return $str.' />';
    }

    /**
     * Form HTML tag start with selectors transfer in params array
     * @param null $params
     * @return string
     */
    public function start($params=NULL)
    {
        if($params){
            foreach($params as $k=>$v){
                $this->formParams[$k] = $v;
            }
        }
        $begin = '<form ';
        foreach($this->formParams as $param=>$value){
            $begin .=$param.'="'.$value.'" ';
        }
        $begin .='>';
        return $begin;
    }

    /**
     * Form close tag and adding hidden input with model primary key id
     * for loaded models
     * @return string
     */
    public function end()
    {
        $str = ($this->exists) ? $this->getHidden($this->exists) : '';
        return $str."\r\n".'</form>';
    }
}