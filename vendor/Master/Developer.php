<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 16.07.16
 * Time: 23:23
 */

namespace Master;


class Developer
{
    public static function dump($data, $kill=NULL)
    {
        echo '<pre>';
        var_dump($data);
        echo '<pre>';
        if($kill)
            exit(1);
    }
}