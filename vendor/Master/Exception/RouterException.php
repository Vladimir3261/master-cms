<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 12.02.16
 * Time: 17:40
 */

namespace Master\Exception;
use Master\Errors;
class RouterException extends MasterException
{
    public function pageNotFound()
    {
        if(!DEBUG){
            Errors::pageNotFound();
        }else{
           parent::display();
        }
    }

    public function badRequest()
    {
        if(!DEBUG){
            Errors::BadRequest();
        }else{
            parent::display();
        }
    }
}