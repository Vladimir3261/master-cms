<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 12.02.16
 * Time: 17:40
 */

namespace Master\Exception;
use Master\Errors;
class DBException extends MasterException
{
    public function __construct($message=NULL, $code=0)
    {
        parent::__construct($message, $code);
        $this->dbError();exit(1);
    }

    public function dbError()
    {
        if(!DEBUG)
        {
            Errors::viewError();
        }
        else
        {
           parent::display(); 
        }
    }
}