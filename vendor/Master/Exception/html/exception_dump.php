<head> 
  <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css"> 
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script> 
</head>
<style>
    .error-message{
        color: #e57373;
        margin: 0 auto;
        display: block;
        width: 100%;
    }
    .container{
        width: 80%;
        height: auto;
        min-height: 150px;
        background-color: rgba(217, 240, 247, 0.48);
        margin: 0 auto;
    }
    .submessages{
        font-family: cursive;
        color: #444444;
        font-size: 20px;
    }
    .inf-stark{
        width: 100%;
        display: inline-block;
        background-color: #e57373;
        color: white;
    }
    .stark-val{
        width: 100%;
        background-color:  #444444;
        color: white;
    }
</style>
<div class="container">
    
<h1 class="error-message"><a href="#"><?=$code?></a> <?=$message?></h1>
<p class="submessages">Stack Trace...<p>



<table class="table table-condensed" width='100%'> 
<tr>
  <td class="success">#</td>
  <td class="success">file</td>
  <td class="success">line</td>
  <td class="success">function</td>
  <td class="success">class</td>
</tr>
<?php $i=1; foreach($array as $k => $v){ /*$file=file($v['file']);*/ ?>
<tr>
  <td class="info"><?=$i?></td>
  <td class="warning"><?=$v['file']?></td>
  <td class="warning"><?=$v['line']?></td>
  <td class="warning"><?=$v['function']?></td>
  <td class="warning"><?=$v['class']?></td>
</tr>
<tr>
    <td colspan="5">
    </td>
</tr>
<?php $i++; } ?>
</table>
<hr/>
</div>