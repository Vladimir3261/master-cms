<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 12.02.16
 * Time: 17:40
 */

namespace Master\Exception;
use Master\Errors;
class ViewException extends MasterException
{
    public function invalidParams()
    {
        if(!DEBUG)
        {
            Errors::viewError();
        }else
        {
           parent::display(); 
        }
    }
}