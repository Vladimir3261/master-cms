<?php
namespace Master;
use Master\Exception\ViewException;
/**
 * base class ViewBase main application class 
 * that is responsible for the output pattern and patterns of access
 * to variables transfer from the controller
 * @package Master
 * @version 1.1
 */
class ViewBase 
{
    protected $params;
    /**
     * @param array | null $params
     * @throws ViewException
     */

    /**
     * view files path
     * @var string
     */
    protected $viewPath;

    /**
     * current render file
     * @var string
     */
    private $viewFile;

    public function __construct($params = null)
    {
        $config = MC::app()->getConfig('config');
        // check input params from controller this must be array required
        try
        {
           if(!is_array($params) && $params !== null)
               throw new ViewException('View params must be array', 1109);
        }
        catch (ViewException $e)
        {
            $e->invalidParams();
            exit(1);
        }
        $this->params = $params;
        $controller = strtolower(str_replace('Controller', '', MC::app()->get('controller')));
        $action = strtolower(str_replace('Action', '', MC::app()->get('action')));
        $this->viewPath = isset($config['viewPath']) ? $config['viewPath'] : ROOTDIR.'/views';
        if( MC::app()->get('layout') )
        {
           $layoutName = MC::app()->get('layout');
        }
        else if( isset($config['layout']) )
        {
            $layoutName = $config['layout'];
        }
        else
        {
            $layoutName = 'default';
        }
        $templateName = isset($config['template']) ? $config['template'] : $template = 'default';
        $ext = isset($config['viewFilesExtension']) ? $config['viewFilesExtension'] : '.phtml';
        $layoutFile = $this->viewPath.DIRSEP.$templateName.DIRSEP.'layout'.DIRSEP.$layoutName.$ext;
        $this->viewFile = (MC::app()->getTemplate() !== NULL) ? $this->viewPath.DIRSEP.$templateName.DIRSEP.MC::app()->getTemplate().$ext
        : $this->viewPath.DIRSEP.$templateName.DIRSEP.$controller.DIRSEP.$action.$ext;
        try
        {
            if(!file_exists($this->viewFile))
                throw new ViewException("View file $this->viewFile not exists", 5666);
            if(!file_exists($layoutFile))
                throw new ViewException("View file $layoutFile not exists", 5667);
        }
        catch(ViewException $e)
        {
            $e->invalidParams();exit(1);
        }
        require_once $layoutFile;
    }

    /**
     * file return template content
     * @return void
     */
    public function content()
    {
        // convert array kes to php variables
        if($this->params !==null)
        {
            foreach($this->params as $k=>$v)
            {
                $$k = $v;
            }
        }
       // include view file
       require_once $this->viewFile;
    }

    /**
     * include css file to render page
     * @param $link string
     * @return string
     */
    public function css($link)
    {
        return '<link rel="stylesheet" type="text/css" href="'. $this->getSiteLink($link) .'">'."\r\n";
    }

    /**
     * include js file to render page
     * @param $link
     * @return string
     */
    public function js($link)
    {
        return '<script src="'. $this->getSiteLink($link) .'"></script>'."\r\n";
    }

    /**
     * get full link to site for access from web. For include css and js files to page
     * @param $link string
     * @return string
     */
    public function getSiteLink($link)
    {
        $config = MC::app()->getConfig('config');
        $request = new Request();
        $request->isHttps() ? $path = 'https://' : $path = 'http://';
        return $path.$config['host'].'/'.$link;
    }

    /**
     * set page title
     * @param null $title
     * @return string
     */
    public function title($title=null)
    {
        $title ? $t = $title : $t = MC::app()->get('title');
        if(!$t){ return ''; }
        return SeoHelper::title($t);
    }

    /**
     * set page description
     * @param $description string|null
     * @return string
     */
    public function description($description=null)
    {
        $description ? $desc = $description : $desc = MC::app()->get('description');
        if(!$desc)
        {
            return SeoHelper::description('');
        }
        return SeoHelper::description($desc);
    }

    /**
     * begin html page and set doctype and accept language
     * HTML5
     * @return string
     */
    public function beginHtml()
    {
        MC::app()->get('language') ?  $lang = MC::app()->get('language') : $lang = 'en';
        return '<!DOCTYPE html>'."\r\n".'<html lang="'.$lang.'">'."\r\n";
    }

    /**
     * @return string
     * close HTML page
     */
    public function endHtml()
    {
        return '</html>';
    }

    /**
     * @return string
     * @param $filename
     */
    public function img($filename)
    {
        return '<img src="'. $this->getSiteLink($filename) .'">'."\r\n";
    }
}