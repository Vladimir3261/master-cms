<?php
namespace Master;
/**
 * Class ControllerBase the base controller for all controllers in system
 * @package Master
 * @version 1.1
 */
Abstract Class ControllerBase extends EventManager
{
    /**
     * @var array $params
     */
    private $params;
    private $em = null;

    /**
     * ControllerBase constructor. Takes parameters when creating an instance
     * of the class and stores them in the $ params to access them
     * from the controller that extends the base class
     * @param array $arr
     */
    public function __construct($arr)
    {
        $this->params = $arr;
    }

    /**
     * indexAction method require to declared
     * @return mixed
     */
    abstract function indexAction();
    
    protected function redirect($redirectLink)
    {
        Header::send(302);
        Header::location($redirectLink);
    }

    public function params($index=null)
    {
        if(!$index){
            return $this->params;
        }else{
            return isset($this->params[$index]) ? $this->params[$index] : null;
        }
    }
    
    /**
     * get doctrine entity manager for models
     * @return \Doctrine\ORM\EntityManager
     */
    public function getEM()
    {
        if(is_null($this->em)){
            $this->em = MC::app()->component('doctrine')->getEntityManager();
        }
        return $this->em;
    }
}