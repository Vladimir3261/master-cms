<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 20.07.16
 * Time: 17:09
 */

namespace Master;


abstract  class AdminController extends ControllerBase
{
    public function __construct(array $arr)
    {
        parent::__construct($arr);
        MC::app()->set('layout', 'admin');
        if(!MC::app()->user()->isAuth() && MC::app()->get('action') !== 'loginAction')
        {
            $this->redirect('/admin/login');exit(1);
        }
    }

    public function indexAction()
    {
        // TODO: Implement indexAction() method.
    }
}