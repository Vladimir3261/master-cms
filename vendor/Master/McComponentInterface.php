<?php
namespace Master;

/**
 * Interface McComponentInterface for all components in system
 * @package Master
 * @version 1.1
 */
interface McComponentInterface 
{
    public function init();
}