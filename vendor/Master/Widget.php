<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 06.04.16
 * Time: 16:14
 */

namespace Master;
use Master\Exception\CoreException;
/**
 * Class Widget provide all widgets in Master-CMS system...
 * Class Widget
 * @package Master
 * @version 1.1
 */
abstract class Widget
{
    protected $params = null;

    /**
     * Widget constructor. save supplied params to $params for access to them from extend class
     * after call required method main.
     * @param $params
     */
    public function __construct($params)
    {
        $this->params = $params;
        $this->main();
    }

    /**
     * rendering widget template with assigned params, from Widgets directory
     * @param $fileName
     * @param null $array
     * @return mixed
     * @throws CoreException
     */
    protected function render($fileName, $array = null)
    {
        $file = ROOTDIR.DIRSEP.'Widgets'.DIRSEP.'views'.DIRSEP.$fileName.'.php';
        if(is_array($array) && count($array))
        {
            foreach($array as $k=>$v)
            {
                $$k = $v;
            }
        }
        try
        {
            if(!is_file($file))
                throw new CoreException("Widget file $file not exists in Widgets/views directory", 1153);
        }
        catch(CoreException $e)
        {
            $e->reset();exit(1);
        }
        require_once $file;
    }

    /**
     * @param null $params
     * Show widget
     * @return mixed
     */
    public static function show($params=null)
    {
        $class = get_called_class();
        new $class($params);
    }

    /**
     * abstract function required to declare this method run the widget code
     * @return mixed
     */
    abstract public function main();

    /**
     * @return string
     * @param $filename
     * @param null|array $params
     */
    public function img($filename, $params=NULL)
    {
        $filename = is_file(ROOTDIR.'/public/'.$filename) ? $filename : '/nophoto.jpg';
        $str = '<img src="'. $this->getSiteLink($filename) .'" ';
        if($params && is_array($params)){
            foreach($params as $k=>$v){
                $str.=$k.'="'. $v .'" ';
            }
        }
        $str.='/>';
        return $str."\r\n";
    }

    /**
     * get full link to site for access from web. For include css and js files to page
     * @param $link string
     * @return string
     */
    public function getSiteLink($link)
    {
        $config = MC::app()->getConfig('config');
        MC::app()->request()->isHttps() ? $path = 'https://' : $path = 'http://';
        return $path.$config['host'].'/'.$link;
    }
}