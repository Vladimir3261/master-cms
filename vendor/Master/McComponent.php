<?php
namespace Master;
/**
 * Class McComponent provide all components in system
 * @version 1.1
 * @package Master
 */
abstract class McComponent extends EventManager implements McComponentInterface
{
    /**
     * constructor call init method this method is required 
     * to implement in child classes
     */
    public function __construct()
    {
        $this->init();
    }
}