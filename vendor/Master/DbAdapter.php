<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 23.05.16
 * Time: 15:09
 */

namespace Master;

use Master\Exception\DBException;
use PDO;
use PDOException;

/**
 * Class DbAdapter
 * @package Master
 * @version 1.0
 */
class DbAdapter
{
    /**
     * Database configuration array
     * @var array|null
     */
    private $config;

    /**
     * Database adapter instance
     * @var Adapter
     */
    private $Adapter;

    /**
     * Set current strategy (select database engine)
     * DbAdapter constructor.
     * @param string $strategy
     */
    public function __construct($strategy='MYSQL')
    {
        $this->config = MC::app()->getConfig('db');
        $this->Adapter = $this->$strategy();
    }

    /**
     * Get created adapter
     * @return Adapter
     */
    public function Adapter()
    {
        return $this->Adapter;
    }
    public function MYSQL()
    {
        try
        {
            if( !isset($this->config['MYSQL']) )
                throw new DBException('MySQL driver for PDO not configured', 998);
        }
        catch(DBException $e)
        {
            $e->dbError();exit(1);
        }
        $host = $this->config['MYSQL']['host'];
        $db = $this->config['MYSQL']['dbname'];
        $charset = $this->config['MYSQL']['charset'];
        $dsn = $dsn = "mysql:host=$host;dbname=$db;charset=$charset";
        $opt = array(
            PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
        );
        try
        {
            return  new PDO($dsn, $this->config['MYSQL']['user'], $this->config['MYSQL']['password'], $opt);
        }
        catch (PDOException $PDO)
        {
            throw new DBException($PDO->getMessage(), $PDO->getCode());
        }
    }

    public function MongoDB()
    {
        return false;
    }

    public function SQLLITE()
    {
        return false;
    }
}