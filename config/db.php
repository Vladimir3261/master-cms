<?php

/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 30.03.16
 * Time: 17:56
 */
return [
    'db' => [
        'MYSQL' => [
            'host'     => 'localhost',
            'driver'   => 'pdo_mysql',
            'user'     => 'root',
            'password' => '123456',
            'dbname'   => 'MASTER',
            'charset'  => 'utf8',
        ],
    ]
];