<?php
/**
 * Event manager configuration
 */
return [
        'EventManager' => [
                'McComponent\EventTestComponent::sayHello' => [
                        'before' => function($name){
                            return [strtoupper($name)];
                        },
                        'after' => function($callableClassResult){
                            return 'MR. '.$callableClassResult;
                        },
                ],
        ]
];