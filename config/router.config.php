<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 04.02.16
 * Time: 19:53
 */
return [
    'router' => [
        'form/{action}/{id}' => 'FormController',
        'master/{action}/{id}/{name}' => 'MarController',
        'master/product/{id}/{name}' => 'MasterController',
    ]
];