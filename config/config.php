<?php
/**
 * The main application configuration file
 */
return [
        'config' => [
                'viewPath' => realpath('../').'/views',
                'layout' => 'default',
                'template' => 'default',
                'viewFilesExtension' => '.phtml',
                'host' => $_SERVER['HTTP_HOST'],
        ],
];