<?php

/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 06.04.16
 * Time: 16:46
 */
namespace Widget;
use Master\Widget;
class TestWidget extends Widget
{
    public function main()
    {
        $name = strtoupper($this->params['name']);
        return $this->render('test', ['name' => $name]);
    }
}